Partners = new Mongo.Collection('partners');


Partners.allow({
    update: function () {
        return true;
    },
    remove: function () {
        return true;
    },
    insert: function() {
        return true;
    }
});

Partners.deny({
    //update: function(userId, post, fieldNames) {
        // may only edit the following two fields:
        //return (_.without(fieldNames, 'responses').length > 0); }
});
