// Global API configuration
var Api = new Restivus({
    prettyJson: true
});

// Generates: GET, POST on /api/items and GET, PUT, DELETE on
// /api/items/:id for the Items collection
//Api.addCollection(Bid);

// Generates: POST on /api/users and GET, DELETE /api/users/:id for
// Meteor.users collection
Api.addCollection(Meteor.users, {
    excludedEndpoints: ['getAll', 'put'],
    routeOptions: {
        authRequired: true
    },
    endpoints: {
        post: {
            authRequired: false
        },
        delete: {
            roleRequired: 'admin'
        }
    }
});

//Maps to: /api/bid/:id
Api.addRoute('bid/:partner',
    {authRequired: false},
    {
        get: function () {
            return Meteor.call("getResponse", this.urlParams.partner);
        },
        post: function () {
            Requests.insert({request: this.bodyParams, timestamp: Date.now(), partner: this.urlParams.partner});
            var deals = Meteor.call("getDealFromRequest", this.bodyParams);
            return Meteor.call("getResponse", this.urlParams.partner, deals);
        }
    });

//Appnexus special api
Api.addRoute('appnexus/auth',
    {authRequired: false},
    {
        get: function () {
            return Appnexus.findOne({endpoint: "auth"}).response;
        },
        post: function () {
            return Appnexus.findOne({endpoint: "auth"}).response;
        }
    });

Api.addRoute('appnexus/publisher',
    {authRequired: false},
    {
        get: function () {
            return Appnexus.findOne({endpoint: "publisher"}).response;
        },
        post: function () {
            return Appnexus.findOne({endpoint: "publisher"}).response;
        }
    });

Api.addRoute('appnexus/ad-profile',
    {authRequired: false},
    {
        get: function () {
            return Appnexus.findOne({endpoint: "adprofile"}).response;
        },
        post: function () {
            return Appnexus.findOne({endpoint: "adprofile"}).response;
        }
    });