if (Appnexus.find().count() === 0) {
    Appnexus.insert({
        endpoint: "publisher", response: {
            "statusCode": "200", "body": {
                "response": {
                    "status": "OK",
                    "count": 1,
                    "id": 123,
                    "start_element": 0,
                    "num_elements": 100,
                    "publisher": {
                        "id": 123,
                        "code": "1",
                        "reselling_name": null,
                        "expose_domains": true,
                        "is_rtb": false,
                        "reselling_exposure": "public",
                        "reselling_exposed_on": "2015-09-23 22:52:10",
                        "timezone": "Europe/Berlin",
                        "last_modified": "2015-09-23 22:52:10",
                        "max_learn_pct": 0,
                        "accept_demand_partner_usersync": null,
                        "learn_bypass_cpm": 5,
                        "ad_quality_advanced_mode_enabled": true,
                        "allow_report_on_default_imps": false,
                        "name": "Publisher_1",
                        "description": "",
                        "state": "active",
                        "default_site_id": 1556173,
                        "default_ad_profile_id": null,
                        "billing_dba": null,
                        "billing_address1": null,
                        "billing_address2": null,
                        "billing_city": null,
                        "billing_state": null,
                        "billing_zip": null,
                        "billing_country": null,
                        "accept_supply_partner_usersync": null,
                        "accept_data_provider_usersync": null,
                        "ym_profile_id": null,
                        "allow_cpm_managed": true,
                        "allow_cpm_external": true,
                        "allow_cpa_managed": true,
                        "allow_cpa_external": false,
                        "allow_cpc_managed": true,
                        "allow_cpc_external": false,
                        "managed_cpc_bias_pct": 100,
                        "managed_cpa_bias_pct": 100,
                        "external_cpc_bias_pct": 100,
                        "external_cpa_bias_pct": 100,
                        "is_oo": false,
                        "base_payment_rule_id": 821904,
                        "base_ad_quality_rule_id": null,
                        "currency": "USD",
                        "visibility_profile_id": null,
                        "cpm_reselling_disabled": false,
                        "cpc_reselling_disabled": false,
                        "platform_ops_notes": null,
                        "pitbull_segment_id": 0,
                        "pitbull_segment_value": 0,
                        "enable_cookie_tracking_default": true,
                        "seller_page_cap_enabled": false,
                        "billing_internal_user": null,
                        "labels": null,
                        "placements": [
                            {
                                "id": 5633717,
                                "code": null
                            }
                        ],
                        "external_inv_codes": null,
                        "contact_info": null,
                        "publisher_brand_exceptions": null
                    },
                    "dbg_info": {
                        "instance": "57.bm-hbapi.prod.lax1",
                        "slave_hit": false,
                        "db": "master",
                        "user::reads": 1,
                        "user::read_limit": 100,
                        "user::read_limit_seconds": 0,
                        "user::writes": 2,
                        "user::write_limit": 60,
                        "user::write_limit_seconds": 0,
                        "reads": 1,
                        "read_limit": 100,
                        "read_limit_seconds": 0,
                        "writes": 2,
                        "write_limit": 60,
                        "write_limit_seconds": 0,
                        "parent_dbg_info": {
                            "instance": "64.bm-hbapi.prod.nym2",
                            "slave_hit": false,
                            "db": "master",
                            "user::reads": 1,
                            "user::read_limit": 100,
                            "user::read_limit_seconds": 0,
                            "user::writes": 2,
                            "user::write_limit": 60,
                            "user::write_limit_seconds": 0,
                            "reads": 1,
                            "read_limit": 100,
                            "read_limit_seconds": 0,
                            "writes": 2,
                            "write_limit": 60,
                            "write_limit_seconds": 0,
                            "awesomesauce_cache_used": false,
                            "count_cache_used": false,
                            "warnings": [],
                            "time": 432.40594863892,
                            "start_microtime": 1443048730.5127,
                            "version": "1.16.181",
                            "slave_lag": 4,
                            "member_last_modified_age": 1443048730,
                            "output_term": "publisher"
                        },
                        "awesomesauce_cache_used": false,
                        "count_cache_used": false,
                        "warnings": [],
                        "time": 632.46417045593,
                        "start_microtime": 1443048730.3646,
                        "version": "1.16.181",
                        "slave_lag": 1,
                        "member_last_modified_age": 1443048730,
                        "output_term": "not_found",
                        "master_instance": "64.bm-hbapi.prod.nym2",
                        "proxy": true,
                        "master_time": 432.40594863892
                    }
                }
            }
        }
    });
    Appnexus.insert({
        endpoint: "adprofile", response: {
            "statusCode": "200", "body": {
                "response": {
                    "status": "OK", "id": "300", "ad-profile": {"id": "300", "publisher_id": "123"}
                }
            }
        }
    });
    Appnexus.insert({
        endpoint: "auth", response: {
            "statusCode": "200", "body": {
                "response": {
                    "status": "OK",
                    "token": "h20hbtptiv3vlp1rkm3ve1qig0"
                }
            }
        }
    });
}