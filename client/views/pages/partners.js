Template.partners.helpers({
    partners: function () {
        var partners = [];
        Partners.find().forEach(function (partner) {
            partners.push({name: partner.partner, responses: partner.responses});
        });
        return partners;
    }
});

Template.newPartnerModal.events({
    "click #createPartner": function () {
        var name = $("#partnerName").val().replace(' ', "-");
        var defaultResponse = {dealId: ["default"], statusCode: "200", body: {text: "Replace with partner body"}};
        Partners.insert({partner: name, "numReqs": 0, responses: [defaultResponse]});
        $("#partnerName").val('');
        $('#newPartner').modal('toggle');
    }
});

Template.response.events({
    "click #edit": function (e, t) {
        $('#editPartner').modal('toggle');
        Session.set("partner", this);
    }, "click #delete": function () {
        $('#deletePartner').modal('toggle');
        Session.set("partner", this);
    }
});


Template.partnerResponse.helpers({
    response: function () {
        return JSON.stringify(this, null, 2);
    }
});



function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    {
        for (var i = 0; i < 5; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
    }
    return text;

}

/*
 ---------------------------------------
 ----------- EDIT PARTNER ------------
 ---------------------------------------
 */

Template.editPartner.events({
    "click #saveChanges": function (e, t) {
        var partnerName = $("#editPartnerName").val().replace(' ', "-");
        var editingPartner = Session.get("partner");
        var partner = Partners.findOne({partner: editingPartner.name});
        var responses = [];
        $('[id^=editResponse]').each(function() {
            responses.push($(this).val());
        });
        Meteor.call("updatePartner", partner._id, partnerName, responses);
        $('#editPartner').modal('toggle');
    },
    "click #addResponse" : function() {
        var editingPartner = Session.get("partner");
        var partner = Partners.findOne({partner: editingPartner.name});
        var defaultResponse = {dealId: [makeid()], statusCode: "200", body: {text: "Replace with partner body"}};
        Meteor.call("addResponseForPartner", partner._id, defaultResponse);
    },
    "click .delete-deal": function(e, t) {
        var editingPartner = Session.get("partner");
        var partner = Partners.findOne({partner: editingPartner.name});
        var dealId = e.target.id.replace("delete-", "");
        Meteor.call("deleteResponse", partner._id, dealId);
    }
});

Template.editPartner.helpers({
    partnerToEdit: function () {
        return Session.get("partner");
    }
});

/*
---------------------------------------
----------- DELETE PARTNER ------------
---------------------------------------
 */


Template.deletePartner.events({
    "click #confirm": function (e, t) {
        var partner = Session.get("partner");
        var id = Partners.findOne({partner: partner.name});
        Meteor.call("deletePartner", id);
        $('#deletePartner').modal('toggle');
    }
});

Template.deletePartner.helpers({
    partner: function () {
        return Session.get("partner").name;
    }
});