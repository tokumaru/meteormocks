Template.requests.helpers({
    requests: function() {
        var reqs = [];
        Requests.find({}, {sort: {timestamp:-1}, limit: 50}).forEach(function(req) {
            reqs.push({request: JSON.stringify(req.request, null, 2), timestamp: (new Date(req.timestamp)).toUTCString(), partner: req.partner});
        });
        return reqs;
    }
});


Template.requests.events({
    "click .clear-link": function() {
        Meteor.call('clearRequests');
    }
});