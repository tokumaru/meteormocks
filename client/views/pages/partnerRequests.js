
Template.configuredPartners.helpers({
    partners: function() {
        var bidEndpoints = [];
        Partners.find().forEach(function(partner) {
            var host = location.protocol + '//' + location.hostname + ":" + location.port;
            var end =  host + "/api/bid/" + partner.partner;
            var name = partner.partner;
            bidEndpoints.push({endpoint: end, numberRequests: partner.numReqs, name: name});
        });
        return bidEndpoints;
    }
});

Template.configuredPartners.events({
    "click #resetCount": function(e, t) {
        Meteor.call("resetRequestCount", this.name);
    }
});
