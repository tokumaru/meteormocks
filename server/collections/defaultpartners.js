if (Partners.find().count() === 0) {
    Partners.insert({
        partner: "ibillboard", numReqs: 0, responses: [{
            "dealId": ["default"], "statusCode": "200", "body": {
                "id": "393", "bidid": "8284", "cur": "US", "customdata": "some custom data", "seatbid": [{
                    "seat": "666018", "group": 0, "bid": [{
                        "id": "bidder id1",
                        "impid": "1",
                        "price": "0.57",
                        "adid": "2822",
                        "adm": "<h1>Deal advert</h1>",
                        "dealid": "deal-id",
                        "crid": "creative id1",
                        "w": "720",
                        "h": "90",
                        "adomain": ["domain1.com", "domain2.com"],
                        "ext": {
                            "avn": "adv1488848261", "agn": "agn881772483", "width": "720", "height": "90"
                        }
                    }, {
                        "id": "bidder id1",
                        "impid": "1",
                        "price": "0.57",
                        "adid": "2822",
                        "adm": "<h1>Open auction advert</h1>",
                        "crid": "creative id1",
                        "w": "720",
                        "h": "90",
                        "adomain": ["domain1.com", "domain2.com"],
                        "ext": {
                            "avn": "adv1488848261", "agn": "agn881772483", "width": "720", "height": "90"
                        }
                    }]
                }]

            }
        }, {
            "dealId": ["dxcwkwxjjm"], "statusCode": "200", "body": {
                "id": "393", "bidid": "8284", "cur": "US", "customdata": "some custom data", "seatbid": [{
                    "seat": "666018", "group": 0, "bid": [{
                        "id": "bidder id1",
                        "impid": "1",
                        "price": "0.57",
                        "adid": "2822",
                        "adm": "<h1>Deal advert</h1>",
                        "dealid": "wrong-deal-id",
                        "crid": "creative id1",
                        "w": "720",
                        "h": "90",
                        "adomain": ["domain1.com", "domain2.com"],
                        "ext": {
                            "avn": "adv1488848261", "agn": "agn881772483", "width": "720", "height": "90"
                        }
                    }, {
                        "id": "bidder id1",
                        "impid": "1",
                        "price": "0.57",
                        "adid": "2822",
                        "adm": "<h1>Open auction advert</h1>",
                        "crid": "creative id1",
                        "w": "720",
                        "h": "90",
                        "adomain": ["domain1.com", "domain2.com"],
                        "ext": {
                            "avn": "adv1488848261", "agn": "agn881772483", "width": "720", "height": "90"
                        }
                    }]
                }]

            }
        }]
    });
    Partners.insert({
        partner: "appnexus", numReqs: 0, responses: [{
            "dealId": ["default"], "statusCode": "200", "body": {
                "external_auction_id": "A4s24536",
                "appnexus_auction_id": 4396943919143109134,
                "request_error": false,
                "no_bid": false,
                "bid": 2.657,
                "creative_id": 345634,
                "landing_page_url": "http://www.landingpage.com",
                "brand_id": 12,
                "buyer_member_id": 328,
                "ad_tag": "<h1>Appnexus OPEN AUCTION ADVERT</h1>"
            }
        }, {
            "dealId": ["deal-id"], "statusCode": "200", "body": {
                "external_auction_id": "A4s24536",
                "appnexus_auction_id": 4396943919143109134,
                "request_error": false,
                "no_bid": false,
                "bid": 2,
                "creative_id": 345634,
                "landing_page_url": "http://www.landingpage.com",
                "brand_id": 12,
                "buyer_member_id": 328,
                "ad_tag": "<h1>Appnexus DEAL ADVERT</h1>"
            }
        }]
    })
}
