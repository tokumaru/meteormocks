Router.configure({
    layoutTemplate: 'mainLayout',
    notFoundTemplate: 'notFound'

});

//
// Example pages routes
//

Router.route('/dashboard', function () {
    this.render('dashboard');
});

Router.route('/partners', function () {
    this.render('partners');
});

Router.route('/templates', function () {
    this.render('responseTemplates');
});

Router.route('/appnexus', function () {
    this.render('appnexus');
});

Router.route('/', function () {
    Router.go('dashboard');
});

