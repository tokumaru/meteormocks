Meteor.methods({
    getDealFromRequest: function(request) {
        var deals = [];
        // Find appnexus deal
        if (request.ext_pub_code !== undefined && isNaN(request.ext_pub_code)) {
            deals.push(request.ext_pub_code);
        }
        // Find all openrtb deals
        if (request.imp !== undefined) {
            _.each(request.imp, function(impr) {
                if(impr.pmp !== undefined && impr.pmp.deals !== undefined) {
                    _.each(impr.pmp.deals, function(deal) {
                        deals.push(deal.id);
                    })
                }
            });
        }
        console.log("Deals found on request: " + deals);
        return deals;
    },
    deleteResponse: function(id, deal) {
        var dealResponse = Partners.findOne({_id: id, "responses.dealId": [deal]}, {fields: {"responses.$": 1}});
        console.log("Deleting response: " + dealResponse);

        Partners.update({_id: id}, {
            $pull:{ responses : dealResponse.responses[0]}
        });
    },
    getResponse: function (id, deals) {
        var partner = Partners.findOne({partner: id});
        if (partner === undefined) {
            return {response: "no endpoint found for: " + id};
        }
        var dealResponse = Partners.findOne({partner: id, "responses.dealId": deals}, {fields: {"responses.$": 1}});
        partner.numReqs++;
        Partners.update(partner._id, {
            $set: {numReqs: partner.numReqs}
        });
        if (dealResponse !== undefined) {
            console.trace("Found deal response for " + deals);
            return dealResponse.responses[0];
        }
        var response = partner.responses[0];
        console.trace("Returning default response: " + response);
        return response;
    },
    resetRequestCount: function(name) {
        var partner = Partners.findOne({partner: name});
        Partners.update(partner._id, {$set: {numReqs: 0}})
    },
    clearRequests: function() {
        Requests.remove({});
    },
    updatePartner: function(partnerId, name, responses) {
        var parsed = [];
        _.each(responses, function(rsp) {
            parsed.push(JSON.parse(rsp));
        });
        Partners.update({_id: partnerId}, {$set: {partner: name, responses: parsed}});
    },
    addResponseForPartner: function(partnerId, response) {
        var updateStatus = Partners.update({_id: partnerId}, {$push: {responses: response}});
        console.log("Updated " + updateStatus + " partner(s).")
    },
    deletePartner: function(partnerId) {
        Partners.remove(partnerId);
    }
});
